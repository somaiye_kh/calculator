package ir.somaiyekhanchezar.mycalculator;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    TextView tv1;
    Button btn1;
    Button btn2;
    Button btn3;
    Button btn4;
    Button btn5;
    Button btn6;
    Button btn7;
    Button btn8;
    Button btn9;
    Button btnZero;
    Button btnPoint;
    Button btnEqual;
    Button btnPlus;
    Button btnMinus;
    Button btnMulti;
    Button btnDivid;
    int num1 = 0;
    String operate = "";
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv1 = (TextView) findViewById(R.id.tv1);
        btn1 = (Button) findViewById(R.id.btn1);
        btn2 = (Button) findViewById(R.id.btn2);
        btn3 = (Button) findViewById(R.id.btn3);
        btn4 = (Button) findViewById(R.id.btn4);
        btn5 = (Button) findViewById(R.id.btn5);
        btn6 = (Button) findViewById(R.id.btn6);
        btn7 = (Button) findViewById(R.id.btn7);
        btn8 = (Button) findViewById(R.id.btn8);
        btn9 = (Button) findViewById(R.id.btn9);
        btnZero = (Button) findViewById(R.id.btn_zero);
        //btnPoint = (Button) findViewById(R.id.btn_point);
        btnEqual = (Button) findViewById(R.id.btn_equal);
        btnPlus = (Button) findViewById(R.id.btn_plus);
        btnMinus = (Button) findViewById(R.id.btn_minus);
        btnMulti = (Button) findViewById(R.id.btn_multi);
        btnDivid = (Button) findViewById(R.id.btn_divid);

        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        btn3.setOnClickListener(this);
        btn4.setOnClickListener(this);
        btn5.setOnClickListener(this);
        btn6.setOnClickListener(this);
        btn7.setOnClickListener(this);
        btn8.setOnClickListener(this);
        btn9.setOnClickListener(this);
        btnZero.setOnClickListener(this);
        //btnPoint.setOnClickListener(this);
        btnEqual.setOnClickListener(this);
        btnPlus.setOnClickListener(this);
        btnMinus.setOnClickListener(this);
        btnMulti.setOnClickListener(this);
        btnDivid.setOnClickListener(this);
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_zero:
                show("0");
                break;
            case R.id.btn1:
                show("1");
                break;
            case R.id.btn2:
                show("2");
                break;
            case R.id.btn3:
                show("3");
                break;
            case R.id.btn4:
                show("4");
                break;
            case R.id.btn5:
                show("5");
                break;
            case R.id.btn6:
                show("6");
                break;
            case R.id.btn7:
                show("7");
                break;
            case R.id.btn8:
                show("8");
                break;
            case R.id.btn9:
                show("9");
                break;
            case R.id.btn_point:
                break;
            case R.id.btn_equal:
                calculate();
                break;
            case R.id.btn_plus:
                operate = "op_plus";
                actReady();
                break;
            case R.id.btn_minus:
                operate = "op_minus";
                actReady();
                break;
            case R.id.btn_multi:
                operate = "op_multi";
                actReady();
                break;
            case R.id.btn_divid:
                operate = "op_divide";
                actReady();
                break;
        }
    }

    private void calculate() {
        int num2 = Integer.parseInt(tv1.getText().toString());
        if (operate == "op_plus") {
            int result = plus(num1, num2);
            show(String.valueOf(result), true);

        } else if (operate == "op_minus") {
            int result = minus(num1, num2);
            show(String.valueOf(result), true);

        } else if (operate == "op_multi") {
            int result = multi(num1, num2);
            show(String.valueOf(result), true);

        } else if (operate == "op_divide") {
            int result = divid(num1, num2);
            show(String.valueOf(result), true);
        }
    }

    private int plus(int a, int b) {
        return a + b;
    }

    private int minus(int a, int b) {
        return a - b;
    }

    private int multi(int a, int b) {
        return a * b;
    }

    private int divid(int a, int b) {
        return a / b;
    }

    private void actReady() {
        num1 = Integer.parseInt(tv1.getText().toString());
        tv1.setText("0");
    }

    private void show(String n, Boolean force) {
        if (force) {
            tv1.setText(n);
        } else {
            String current = tv1.getText().toString();
            if (current.equals("0")) {
                tv1.setText(n);
            } else {
                tv1.append(n);
            }
        }
    }

    private void show(String n) {
        String current = tv1.getText().toString();
        if (current.equals("0")) {
            tv1.setText(n);
        } else {
            tv1.append(n);
        }
    }


    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://ir.somaiyekhanchezar.mycalculator/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://ir.somaiyekhanchezar.mycalculator/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }
}
